#pragma once

template<class T> class BinarySearchTreeNode
{
	private:
		T data;
		BinarySearchTreeNode* parent;
		BinarySearchTreeNode* right;
		BinarySearchTreeNode* left;
	public:
		BinarySearchTreeNode(T data) : data(data) {}
		void setRight(BinarySearchTreeNode* right) 
		{
			this->right = right;
		}
		void setLeft(BinarySearchTreeNode* left)
		{
			this->left = left;
		}
		void setParent(BinarySearchTreeNode* parent)
		{
			this->parent = parent;
		}
		void setData(T data)
		{
			this->data = data;
		}
		BinarySearchTreeNode* getLeft()
		{
			return left;
		}
		BinarySearchTreeNode* getRight()
		{
			return right;
		}
		BinarySearchTreeNode* getParent()
		{
			return parent;
		}
		T getData()
		{
			return data;
		}
};

