#pragma once
#include "priorityQueue.h"
#include "LinkedList.h"

template<class T> class LinkedListPriorityQueue :
	public PriorityQueue<T>, public LinkedList<T>
{
public:
public:
	virtual void Enqueue(T data)
	{
		this->Insert(data);
	}
	virtual T Dequeue()
	{
		LinkedListNode<T>* biggestValueNode = this->getHead();
		this->remove(biggestValueNode);
		return biggestValueNode->getData();
	}
	virtual bool IsEmpty()
	{
		return !LinkedList<T>::head;
	}
};

