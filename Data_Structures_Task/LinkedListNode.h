#pragma once
template<class T> class LinkedListNode
{
private:
	T data;
	LinkedListNode* next;
public:
	LinkedListNode();
	~LinkedListNode();

	LinkedListNode(T data, LinkedListNode* next) : data(data), next(next) {}
	LinkedListNode(T data) : data(data) {}

	void setNext(LinkedListNode* next)
	{
		this->next = next;
	}
	
	void setData(T data)
	{
		this->data = data;
	}
	
	LinkedListNode* getNext()
	{
		return next;
	}
	
	T getData()
	{
		return data;
	}
};

