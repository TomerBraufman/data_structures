#pragma once
#include "BinarySearchTreeNode.h"

template<class T> class BinarySearchTree
{
	protected:
		BinarySearchTreeNode<T>* head;
	public:
		BinarySearchTreeNode<T>* Search(T value, BinarySearchTreeNode<T>* start)
		{
			if (start == nullptr || value == start->getData())
				return start;
			if (value < start->getData())
				return Search(start->getLeft(), value);
			else return Search(start->getRight() ,value);
		}
		void Insert(BinarySearchTreeNode<T>* toInsert, BinarySearchTreeNode<T>* start)
		{
			BinarySearchTreeNode<T> *pos = nullptr, *forward = start;
			while (forward)
			{
				pos = forward;
				if (toInsert->getData() < forward->getData())
				{
					forward = forward->getLeft();
				}
				else
				{
					forward = forward->getRight();
				}
			}
			if (pos)
			{
				toInsert->setParent(pos);
				if (toInsert->getData() < pos->getData())
				{
					pos->setLeft(toInsert);
				}
				else
				{
					pos->setRight(toInsert);
				}
			}
			else
				head = toInsert;
		}
		BinarySearchTreeNode<T>* Delete(BinarySearchTreeNode<T>* toDelete, BinarySearchTreeNode<T>* start)
		{
			BinarySearchTreeNode<T> *x, *y;
			if (toDelete->getLeft() == nullptr || toDelete->getRight() == nullptr)
				y = toDelete;
			else
				y = Successor(toDelete);
			if (y->getLeft())
				x = y->getLeft();
			else
				x = y->getRight();
			if (x)
				x->setParent(y->getParent());
			if (!y->getParent())
				head = x;
			else if (y == y->getParent()->getLeft())
				y->getParent()->setLeft(x);
			else
				y->getParent()->setRight(x);
			if (y != toDelete)
				toDelete->setData(y->getData());
			return y;
		}
		BinarySearchTreeNode<T>* Max(BinarySearchTreeNode<T>* start)
		{
			BinarySearchTreeNode<T>* largest = start;
			while (largest->getRight())
			{
				largest = largest->getRight();
			}
			return largest;
		}
		BinarySearchTreeNode<T>* Min(BinarySearchTreeNode<T>* start)
		{
			BinarySearchTreeNode<T>* largest = start;
			while (largest->getLeft())
			{
				largest = largest->getLeft();
			}
			return largest;
		}
		BinarySearchTreeNode<T>* Successor(BinarySearchTreeNode<T>* start)
		{
			if (start->getRight())
			{
				return Min(start->getRight());
			}
			BinarySearchTreeNode<T>* y = start->getParent();
			while (y != nullptr && start == y->getRight())
				start == y;
			return y->getParent();
		}
		BinarySearchTreeNode<T>* Predecessor(BinarySearchTreeNode<T>* start)
		{
			if (start->getLeft())
			{
				return Min(start->getLeft());
			}
			BinarySearchTreeNode<T>* y = start->getParent();
			while (y != nullptr && start == y->getLeft())
				start == y;
			return y->getParent();
		}

};
