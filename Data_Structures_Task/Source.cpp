#include <iostream>
#include <time.h>
#include "BinarySearchTreePriorityQueue.h"
#include "LinkedListPriorityQueue.h"
using namespace std;
int main()
{
	PriorityQueue<int> *bst = new BinarySearchTreePriorityQueue<int>(), *ll = new LinkedListPriorityQueue<int>();
	int temp;
	srand(time(0));
	for (int i = 0; i < 50; i++)
	{
		temp = (rand() % 1000) + 1;
		//bst->Enqueue(temp);
		ll->Enqueue(temp);
		cout << temp << endl;
	}

	cout << "------------------------" << endl;

	for (int i = 0; i < 50; i++)
	{
		cout << ll->Dequeue() << endl;
	}
	for (int i = 0; i < 50; i++)
	{
		//cout << bst->Dequeue() << endl;
	}
	cout << "LinkedList isEmpty" << ll->IsEmpty() << endl;
	//cout << "BinarySearchTree isEmpty" << bst->IsEmpty() << endl;
	return 0;
}