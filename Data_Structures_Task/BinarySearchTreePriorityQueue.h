#pragma once
#include "priorityQueue.h"
#include "BinarySearchTree.h"

template<class T> class BinarySearchTreePriorityQueue :
	public PriorityQueue<T>, public BinarySearchTree<T>
{
	public:
		virtual void Enqueue(T data) 
		{
			this->Insert(new BinarySearchTreeNode<T>(data), BinarySearchTree<T>::head);
		}
		virtual T Dequeue()
		{
			BinarySearchTreeNode<T>* node = this->Max(BinarySearchTree<T>::head);
			this->Delete(node, BinarySearchTree<T>::head);
			return node->getData();
		}
		virtual bool IsEmpty()
		{
			return !BinarySearchTree<T>::head;
		}
};

