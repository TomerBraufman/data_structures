#pragma once
#include "LinkedListNode.h"

template<class T> class LinkedList
{
protected:
	LinkedListNode<T>* head;
	LinkedListNode<T>* last;

public:
	void Insert(T data)
	{
		if (head == nullptr)
		{
			head = last = new LinkedListNode<T>(data);
			head->setNext(last);
			last->setNext(head);
		}

		else if (head->getData() <= data)
		{
			LinkedListNode<T>* newNode = new LinkedListNode<T>(data, head);
			head = newNode;
			last->setNext(newNode);
		}

		else
		{
			LinkedListNode<T>* pos = head->getNext();
			LinkedListNode<T>* prev = head;

			while (pos->getData() > data && pos != last)
			{
				prev = pos;
				pos = pos->getNext();
			}

			if (pos->getData() <= data)
			{
				LinkedListNode<T>* newNode = new LinkedListNode<T>(data, pos);
				prev->setNext(newNode);
			}
			else
			{
				LinkedListNode<T>* newNode = new LinkedListNode<T>(data, head);
				last->setNext(newNode);
				last = newNode;
			}
		}
	}

	int getLength()
	{
		if (head == nullptr)
			return 0;

		int sum = 0;
		LinkedListNode<T>* pos = head;

		while (pos != last)
		{
			sum++;
			pos = pos->getNext();
		}

		// For the last node
		sum++;

		return sum;
	}

	LinkedListNode<T>* getHead()
	{
		return this->head;
	}

	void remove(LinkedListNode<T>* nodeToRemove)
	{
		if (head == nullptr)
			return;

		if (head == nodeToRemove)
		{
			if (head == last)
				head = last = nullptr;
			else
			{
				head = head->getNext();
				last->setNext(head);
			}
		}

		LinkedListNode<T>* pos = head;
		LinkedListNode<T>* prev = head;


		while (pos != nodeToRemove && pos != last)
		{
			prev = pos;
			pos = pos->getNext();
		}

		if (pos == nodeToRemove)
		{
			prev->setNext(pos->getNext());
			if (pos == last)
				last = prev;
		}
	}
};
