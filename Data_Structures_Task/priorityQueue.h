#pragma once

template<class T> class PriorityQueue
{
	public:
		virtual T Dequeue() = 0;
		virtual void Enqueue(T data) = 0;
		virtual bool IsEmpty() = 0;
};

